import pickle


PICKLE_FILE = r'rf_preds.pkl'
UNIQUE_MAL = r'mal_ip_test_indices.pkl'


def load_precision_one(pickle_file=PICKLE_FILE):
    """Load the file containing past predicitions that had precision of one
    to ensure that they are still always flagged as ones that we previously
    flagged as one"""
    correct_flag = pickle.load(open(pickle_file, 'rb'))
    return correct_flag


def update_already_correct(dict_to_check):
    """Make sure the dict_to_check has true for indices we know are true"""
    true_dict = load_precision_one(PICKLE_FILE)
    mal_ip = load_precision_one(UNIQUE_MAL)
    print("True Flagged Events: {:,}".format(sum(true_dict.values())))
    print("Pre Check Flagged Events: {:,}".format(sum(dict_to_check.values())))
    true_flags = [k for k, v in true_dict.items() if v]
    try:
        for true_ind in true_flags:
            dict_to_check[true_ind] = True
        print("Post Check True Flagged Events: {:,}".format(sum(dict_to_check.values())))
        for mal_ind in mal_ip:
            dict_to_check[mal_ind] = True
        print("Post Check Mal Flagged Events: {:,}".format(sum(dict_to_check.values())))
    except KeyError as e:
        print(e)
    return dict_to_check


def get_mal_only_ips(df_test, df_mal, df_benign):
    """Gets the ips that were unique to the malicious attacks and
    finds the those source ips in the test dataframe and keeps the index"""
    mal_ips = set(df_mal.src_ip)
    benign_ips = set(df_benign.src_ip)
    unique_mal = mal_ips - benign_ips
    test_indices = [i for i, v in enumerate(df_test.src_ip) if v in (unique_mal)]
    return test_indices


def pickle_object(thing_to_pickle, pickle_filename):
    """Saves something in the given pickle file"""
    pickle.dump(thing_to_pickle, open(pickle_filename, 'wb'))


def test():
    """Test run to make sure runs well, should end with 1 more flagged"""
    cf = load_precision_one()
    cf[1049] = False
    cf[121] = True
    update_already_correct(cf)


if __name__ == '__main__':
    pass
