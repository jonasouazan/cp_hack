import ipaddress

"""Add all your WORKING functions here"""
def port_features(dataframe):
    """Returns the dataframe with an added feature of whether the port was the DNS port"""
    dataframe['is_port_53'] = dataframe['dst_port'] == 53
    dataframe['is_port_80'] = dataframe['dst_port'] == 80
    return dataframe

def add_ip_features(dataframe):
    """Returns the dataframe with added columns defining if the source/dest ip is public or private 
    as well as the pair"""
    def type_ip(x):
        try:
            if ipaddress.ip_address(x).is_private:
                return 'Private'
            else:
                return 'Public'
        except ValueError:
            return 'invalid'

    dataframe['dst_type'] = dataframe['dst_ip'].apply(type_ip)
    dataframe['src_type'] = dataframe['src_ip'].apply(type_ip)
    dataframe['ip_pair'] = dataframe['src_type'] + dataframe['dst_type']
    dataframe = dataframe.drop(dataframe[dataframe['dst_type'] == 'invalid'].index)
    
    return dataframe

def udp_feature(dataframe):
    """Returns the dataframe with an added column of whether or not the port was UDP"""
    dataframe['is_UDP'] = dataframe['protocol'] == 'UDP'
    return dataframe


import re
import pandas as pd


def ip_network_class(ip):
    """Takes an IP address and returns the class of the IP"""
    try:
        pattern = r'([\d]+).([\d]+).([\d]+).([\d]+)'
        match = re.search(pattern, ip)
        ip_key = int(match.group(1))
        if ip_key < 127:
            return 'A'
        elif ip_key < 192:
            return 'B'
        elif ip_key < 224:
            return 'C'
        elif ip_key < 240:
            return 'D'
        elif ip_key < 256:
            return 'E'
        return
    except:
        return 0


def get_network_host(ip, host=False):
    """Returns the network portion of the IP address or the host portion of the ip address"""
    try:
        pattern = r'([\d]+).([\d]+).([\d]+).([\d]+)'
        match = re.search(pattern, ip)
        ip_key = int(match.group(1))
        if ip_key < 127:
            if host:
                return re.search(r'([\d]+).([\d]+.[\d]+.[\d]+)', ip).group(2)
            match = re.search(r'([\d]+).([\d]+).([\d]+).([\d]+)', ip).group(1)
            return match
        elif ip_key < 192:
            if host:
                return re.search(r'([\d]+.[\d]+).([\d]+.[\d]+)', ip).group(2)
            match = re.search(r'([\d]+.[\d]+).([\d]+).([\d]+)', ip).group(1)
            return match
        elif ip_key < 224:
            if host:
                return re.search(r'([\d]+.[\d]+.[\d]+).([\d]+)', ip).group(2)
            match = re.search(r'([\d]+.[\d]+.[\d]+).([\d]+)', ip).group(1)
            return match
        return 0
    except:
        return 0


def zero_sent_received(df):
    """Returns the dataframe with an added column indicating if the recieved and sent
     bytes are both zero"""
    df['ZeroSentReceived'] = (df.received_bytes + df.sent_bytes) == 0
    return df


def target_network(df):
    """Returns the dataframe with an added column indicating if the recieved and sent
     bytes are both zero"""
    df['DestNetwork10'] = df.destNetwork == '10'
    df['DestNetwork8'] = df.destNetwork == '8'
    df['DestNetwork70'] = df.destNetwork == '70'
    return df


def client_feature(df):
    df['sourceClass'] = df.src_ip.apply(ip_network_class)
    df['destClass'] = df.dst_ip.apply(ip_network_class)
    df['Client5SourceClassB'] = (df.client == 5) & (df.sourceClass == 'B')
    df['Client3DestClassC'] = (df.client == 3) & (df.destClass == 'C')
    df['Client5PublicPrivate'] = (df.client == 5) & (df.ip_pair == 'PublicPrivate')
    df['Client3PrivatePrivate'] = (df.client == 3) & (df.ip_pair == 'PrivatePrivate')
    return df

def network_properties(df):
    """Create columns related to the source and destination IP adresses:
    - The type of class of each IP
    - The network portion of the IP
    - the Host portion of the IP"""
    source_cols = list(df.columns)
    df['sourceClass'] = df.src_ip.apply(ip_network_class)
    df['destClass'] = df.dst_ip.apply(ip_network_class)
    df['sourceNetwork'] = df.src_ip.apply(get_network_host)
    df['destNetwork'] = df.dst_ip.apply(get_network_host)
    df['sourceHost'] = df.src_ip.apply(lambda x: get_network_host(x, True))
    df['destHost'] = df.dst_ip.apply(lambda x: get_network_host(x, True))
    df = pd.get_dummies(df, columns=['sourceClass', 'destClass'])
    new_cols = source_cols + ['sourceClass_A', 'sourceClass_B', 'sourceClass_C', 'destClass_A', 'destClass_B', 'destClass_C',
                             'sourceNetwork', 'destNetwork', 'sourceHost', 'destHost']
    extract_cols = [c for c in new_cols if c in df.columns]
    return df[extract_cols]

def calculate_traffic_metrics(df):
    df_c = df.copy().set_index(["client", "src_ip"])
    inbound_traffic_share = df.groupby(df.client).src_ip.value_counts() / df.groupby(df.client).src_ip.count()
    outbound_traffic_share = df.groupby(df.client).dst_ip.value_counts() / df.groupby(df.client).dst_ip.count()
    df_c["inbound_share"] = inbound_traffic_share
    df_c = df_c.reset_index().set_index(["client", "dst_ip"])
    df_c["outbound_share"] = outbound_traffic_share
    df_c = df_c.reset_index()
    df_c["talks_to_server"] = df_c.groupby(df.client).apply(lambda x: x["outbound_share"] * 3 > x["outbound_share"].std()).reset_index().drop(columns=["client", "level_1"])
    return df_c
