# The main features for each model with its importance
# The ROC curve for each model and the recall vs precision curve
# Also precision and recall over time
# A bar that moves the threshold, modifying the graphs
# The score distribution in the test and train set in separate graphs
# Plot of every sample with TSNE (Use the label to color the samples)

from bokeh.plotting import figure
from bokeh.io import curdoc
from bokeh.models import HoverTool
from bokeh.models import ColumnDataSource
from bokeh.models.widgets import Div, Dropdown
from bokeh.layouts import widgetbox, column, row, layout
from bokeh.models.widgets import DataTable, DateFormatter, TableColumn, NumberFormatter, Slider

import pandas as pd
import numpy as np
from sklearn.metrics import precision_recall_curve, roc_curve
import os
import pickle
import itertools


class Dashboard(object):
    def __init__(self, model1, model2):
        ordered_importance = sorted([(f, v) for f, v in zip(model1['column_names'], model1['feature_importances'])],
                                    key=lambda x: x[1], reverse=False)
        self.rf_feature_importance = [x[1] for x in ordered_importance]
        self.rf_test_prob = list(model1['predict_probas_test'][:, 1])
        self.rf_train_prob = list(model1['predict_probas_train'][:, 1])
        self.rf_train_true = list(model1['train_labels'])
        self.rf_val_prob = list(model1['predict_probas_val'][:, 1])
        self.rf_val_true = list(model1['val_labels'])
        self.rf_features = [x[0] for x in ordered_importance]
        self.rf_threshold = 0.8
        ordered_importance = sorted([(f, v) for f, v in zip(model2['column_names'], model2['feature_importances'])],
                                    key=lambda x: x[1], reverse=False)
        self.xg_feature_importance = [x[1] for x in ordered_importance]
        self.xg_test_prob = list(model2['predict_probas_test'][:, 1])
        self.xg_train_prob = list(model2['predict_probas_train'][:, 1])
        self.xg_train_true = list(model2['train_labels'])
        self.xg_val_prob = list(model2['predict_probas_val'][:, 1])
        self.xg_val_true = list(model2['val_labels'])
        self.xg_features = [x[0] for x in ordered_importance]
        self.xg_threshold = 0.75
        self.calculate_summary()

    def run(self):
        """The main method to run the server"""
        # Set-up the data and dropdowns
        # Create the components for display
        title = Div(width=800)
        title.text = "<h1>Checkpoint Models</h1>"
        # Model Selections and Thresholds
        train_threshold_title = Div(width=700)
        train_threshold_title.text = "<h3>Train Data Threshold Analysis</h3>"
        rf_threshold_slider = Slider(start=0, end=1, value=0.8, step=.01,
                               title="Random Forest Threshold", width=500)
        xg_threshold_slider = Slider(start=0, end=1, value=0.75, step=.01,
                               title="XGBoost Threshold", width=500)

        # Feature Title
        feature_importance_title = Div(width=700)
        feature_importance_title.text = "<h3>Feature Importance by Model</h3>"
        #RF Feature Plot
        rf_features = ColumnDataSource(data={'features': self.rf_features, 'feature_importance': self.rf_feature_importance})

        rf_importance_plot = figure(y_range=self.rf_features, width=600, height=500,
                                 title="Random Forest Feature Importance")
        rf_importance_plot.hbar(y='features', right='feature_importance', height=0.9, source=rf_features)
        # Set some properties to make the plot look better
        rf_importance_plot.ygrid.grid_line_color = None
        rf_importance_plot.xaxis.axis_label = "Feature Importance"
        rf_importance_plot.yaxis.axis_label = "Feature"
        rf_importance_plot.add_tools(HoverTool(tooltips=[("Feature", "$y"), ("Importance", "$x")]))

        #XG Feature Plot
        xg_features = ColumnDataSource(data={'features': self.xg_features, 'feature_importance': self.xg_feature_importance})

        xg_importance_plot = figure(y_range=self.xg_features, width=600, height=500,
                                 title="XGBoost Feature Importance")
        xg_importance_plot.hbar(y='features', right='feature_importance', height=0.9, source=xg_features)
        # Set some properties to make the plot look better
        xg_importance_plot.ygrid.grid_line_color = None
        xg_importance_plot.xaxis.axis_label = "Feature Importance"
        xg_importance_plot.yaxis.axis_label = "Feature"
        xg_importance_plot.add_tools(HoverTool(tooltips=[("Feature", "$y"), ("Importance", "$x")]))

        # Probability Distribution
        train_probability_title = Div(width=700)
        train_probability_title.text = "<h3>Distribution of Train Probabilities</h3>"
        # Train Histogram
        bin_size = 0.05
        rf_assigned_bin = self.generate_bins(self.rf_train_prob, bin_size)
        # https://datascience.stackexchange.com/questions/10322/how-to-plot-multiple-variables-with-pandas-and-bokeh
        rf_train_prob_plot = figure(title="Train Probability Distribution", tools='', width=450, height=450)
        rf_train_df = pd.DataFrame(data={'label': self.rf_train_true, 'bin': rf_assigned_bin, 'prob': self.rf_train_prob})
        rf_group = rf_train_df.groupby(by=['bin', 'label']).prob.count().unstack()
        rf_group = rf_group.fillna(0).reset_index()
        rf_group.columns = ['bin', 'False', 'True']
        rf_train_plot_data = ColumnDataSource(data=rf_group)
        rf_train_prob_plot.vbar_stack(['False', 'True'], x='bin', width=bin_size, alpha=0.25, color=["blue", "red"],
                                   source=rf_train_plot_data)
        # Data Table
        table_columns = [TableColumn(field="bin", title="bin", formatter=NumberFormatter(format='0.000')),
                         TableColumn(field="False", title="Class 0", formatter=NumberFormatter(format='0,0')),
                         TableColumn(field="True", title="Class 1", formatter=NumberFormatter(format='0,0'))]
        rf_train_prob_data_table = DataTable(source=rf_train_plot_data, columns=table_columns, width=200)

        #XGBoost Distribution
        xg_assigned_bin = self.generate_bins(self.xg_train_prob, bin_size)
        # https://datascience.stackexchange.com/questions/10322/how-to-plot-multiple-variables-with-pandas-and-bokeh
        xg_train_prob_plot = figure(title="Train Probability Distribution", tools='', width=450, height=450)
        xg_train_df = pd.DataFrame(data={'label': self.xg_train_true, 'bin': xg_assigned_bin, 'prob': self.xg_train_prob})
        xg_group = xg_train_df.groupby(by=['bin', 'label']).prob.count().unstack()
        xg_group = xg_group.fillna(0).reset_index()
        xg_group.columns = ['bin', 'False', 'True']
        xg_train_plot_data = ColumnDataSource(data=xg_group)
        xg_train_prob_plot.vbar_stack(['False', 'True'], x='bin', width=bin_size, alpha=0.25, color=["blue", "red"],
                                   source=xg_train_plot_data)
        # Data Table
        table_columns = [TableColumn(field="bin", title="bin", formatter=NumberFormatter(format='0.000')),
                         TableColumn(field="False", title="Class 0", formatter=NumberFormatter(format='0,0')),
                         TableColumn(field="True", title="Class 1", formatter=NumberFormatter(format='0,0'))]
        xg_train_prob_data_table = DataTable(source=xg_train_plot_data, columns=table_columns, width=200)

        # Summary Text
        random_summary = Div(width=700)
        xgboost_summary = Div(width=700)
        random_val_summary = Div(width=700)
        xgboost_val_summary = Div(width=700)
        random_summary.text, xgboost_summary.text, random_val_summary.text, xgboost_val_summary.text = self.get_summary_text()

        # PR Curve for train
        rf_precision, rf_recall, rf_thresholds = precision_recall_curve(self.rf_train_true, self.rf_train_prob)
        xg_precision, xg_recall, xg_thresholds = precision_recall_curve(self.xg_train_true, self.xg_train_prob)
        rf_val_precision, rf_val_recall, rf_val_thresholds = precision_recall_curve(self.rf_val_true, self.rf_val_prob)
        xg_val_precision, xg_val_recall, xg_val_thresholds = precision_recall_curve(self.xg_val_true, self.xg_val_prob)
        # Precision Recall F1 Score Graphs
        recall = figure(title="Precision Recall Graph Train", width=600, height=500)
        recall.line(rf_recall, rf_precision, line_width=2, line_color='blue', line_dash="dotdash", legend="Random")
        recall.line(xg_recall, xg_precision, line_width=2, line_color='orange', line_dash="dotdash", legend="XGBoost")
        recall.xaxis.axis_label = "Recall"
        recall.yaxis.axis_label = "Precision"
        recall.legend.location = "bottom_right"
        recall.add_tools(HoverTool(tooltips=[
            ("Recall", "$x"),
            ("Precision", "$y")]))
        # Validation Recall
        val_recall = figure(title="Precision Recall Graph Validation", width=600, height=500)
        val_recall.line(rf_val_recall, rf_val_precision, line_width=2, line_color='blue', line_dash="dotdash", legend="Random")
        val_recall.line(xg_val_recall, xg_val_precision, line_width=2, line_color='orange', line_dash="dotdash", legend="XGBoost")
        val_recall.xaxis.axis_label = "Recall"
        val_recall.yaxis.axis_label = "Precision"
        val_recall.legend.location = "bottom_right"
        val_recall.add_tools(HoverTool(tooltips=[
            ("Recall", "$x"),
            ("Precision", "$y")]))

        def update(attr, old, new):
            """Update text and data sources when date filter changes"""
            self.rf_threshold = rf_threshold_slider.value
            self.xg_threshold = xg_threshold_slider.value
            self.calculate_summary()
            random_summary.text, xgboost_summary.text, random_val_summary.text, xgboost_val_summary.text = self.get_summary_text()

        rf_threshold_slider.on_change('value', update)
        xg_threshold_slider.on_change('value', update)
        # Titles for the data tables
        random_table_title = Div(width=700)
        random_table_title.text = "<h3>Random Forest Statistics</h3>"
        xgboost_table_title = Div(width=700)
        xgboost_table_title.text = "<h3>XGBoost Statistics</h3>"
        # Build the layout
        # doc_layout.children.append(row(random_plot, xgboost_plot))
        doc_layout = layout(sizing_mode='scale_both')
        doc_layout.children.append(row(title))
        doc_layout.children.append(row(feature_importance_title))
        doc_layout.children.append(row(rf_importance_plot, xg_importance_plot))
        doc_layout.children.append(row(train_threshold_title))
        doc_layout.children.append(row(column(widgetbox(rf_threshold_slider, width=650), random_summary, random_val_summary),
                                       column(xg_threshold_slider, xgboost_summary, xgboost_val_summary)))
        #doc_layout.children.append(row(widgetbox(model_dropdown, width=650), threshold_slider))
        doc_layout.children.append(row(rf_train_prob_plot, rf_train_prob_data_table, xg_train_prob_plot, xg_train_prob_data_table))
        doc_layout.children.append(row(recall, val_recall))
        # Generate layout
        curdoc().add_root(doc_layout)

    def calculate_summary(self):
        """Calculates the metrics for each model"""
        rf_my_positive = [i for i, v in enumerate(self.rf_train_prob) if v >= self.rf_threshold]
        xg_my_positive = [i for i, v in enumerate(self.xg_train_prob) if v >= self.xg_threshold]
        # Count of true positives and false positives
        rf_picked_positives = [v for i, v in enumerate(self.rf_train_true) if i in rf_my_positive]
        xg_picked_positives = [v for i, v in enumerate(self.xg_train_true) if i in xg_my_positive]
        rf_tp = sum(rf_picked_positives)
        rf_fp = len(rf_picked_positives) - sum(rf_picked_positives)
        xg_tp = sum(xg_picked_positives)
        xg_fp = len(xg_picked_positives) - sum(xg_picked_positives)
        # divide by zero error earlier
        self.rf_precision = 0
        if (rf_fp + rf_tp) > 0:
            self.rf_precision = rf_tp / (rf_fp + rf_tp)
        self.xg_precision = 0
        if (xg_fp + xg_tp) > 0:
            self.xg_precision = xg_tp / (xg_fp + xg_tp)
        # Recall TP /
        rf_all_pos = sum(self.rf_train_true)
        xg_all_pos = sum(self.xg_train_true)
        self.rf_recall = 0
        if rf_all_pos > 0:
            self.rf_recall = rf_tp / rf_all_pos
        self.xg_recall = 0
        if xg_all_pos > 0:
            self.xg_recall = xg_tp / xg_all_pos
        self.rf_f1 = 0
        if self.rf_recall > 0 or self.rf_precision > 0:
            self.rf_f1 = self.rf_recall * self.rf_precision * 2 / (self.rf_recall + self.rf_precision)
        self.xg_f1 = 0
        if self.xg_recall > 0 or self.xg_precision > 0:
            self.xg_f1 = self.xg_recall * self.xg_precision * 2 / (self.xg_recall + self.xg_precision)
        # Validation Numbers
        rf_my_positive = [i for i, v in enumerate(self.rf_val_prob) if v >= self.rf_threshold]
        xg_my_positive = [i for i, v in enumerate(self.xg_val_prob) if v >= self.xg_threshold]
        # Count of true positives and false positives
        rf_picked_positives = [v for i, v in enumerate(self.rf_val_true) if i in rf_my_positive]
        xg_picked_positives = [v for i, v in enumerate(self.xg_val_true) if i in xg_my_positive]
        rf_tp = sum(rf_picked_positives)
        rf_fp = len(rf_picked_positives) - sum(rf_picked_positives)
        xg_tp = sum(xg_picked_positives)
        xg_fp = len(xg_picked_positives) - sum(xg_picked_positives)
        # divide by zero error earlier
        self.rf_val_precision = 0
        if (rf_fp + rf_tp) > 0:
            self.rf_val_precision = rf_tp / (rf_fp + rf_tp)
        self.xg_val_precision = 0
        if (xg_fp + xg_tp) > 0:
            self.xg_val_precision = xg_tp / (xg_fp + xg_tp)
        # Recall TP /
        rf_all_pos = sum(self.rf_val_true)
        xg_all_pos = sum(self.xg_val_true)
        self.rf_val_recall = 0
        if rf_all_pos > 0:
            self.rf_val_recall = rf_tp / rf_all_pos
        self.xg_val_recall = 0
        if xg_all_pos > 0:
            self.xg_val_recall = xg_tp / xg_all_pos
        self.rf_val_f1 = 0
        if self.rf_val_recall > 0 or self.rf_val_precision > 0:
            self.rf_val_f1 = self.rf_val_recall * self.rf_val_precision * 2 / (self.rf_val_recall + self.rf_val_precision)
        self.xg_val_f1 = 0
        if self.xg_val_recall > 0 or self.xg_val_precision > 0:
            self.xg_val_f1 = self.xg_val_recall * self.xg_val_precision * 2 / (self.xg_val_recall + self.xg_val_precision)



    @staticmethod
    def generate_bins(probabilities, bin_size):
        assigned_bin = [(p // bin_size)*bin_size + (bin_size / 2) for p in probabilities]
        return assigned_bin

    def get_summary_text(self):
        """Creates the text that is displayed on the site"""
        rf_text = """
                            <b style="color:green">Random Forest Train: </b>
                            <p> Threshold: {:,.3f}  -  Precision: {:,.3f} -  Recall: {:,.3f}  -  F1 Score: {:,.3f}  -  Total Samples: {:,.0f}</p>
                            """.format(self.rf_threshold, self.rf_precision, self.rf_recall, self.rf_f1, len(self.rf_train_true))
        xg_text = """
                            <b style="color:orange">XGBoost Train: </b>
                            <p> Threshold: {:,.3f}  -  Precision: {:,.3f} -  Recall: {:,.3f}  -  F1 Score: {:,.3f}  -  Total Samples: {:,.0f}</p>
                            """.format(self.xg_threshold, self.xg_precision, self.xg_recall, self.xg_f1, len(self.xg_train_true))
        rf_val_text = """
                            <b style="color:green">Random Forest Validation: </b>
                            <p> Threshold: {:,.3f}  -  Precision: {:,.3f} -  Recall: {:,.3f}  -  F1 Score: {:,.3f}  -  Total Samples: {:,.0f}</p>
                            """.format(self.rf_threshold, self.rf_val_precision, self.rf_val_recall, self.rf_val_f1, len(self.rf_val_true))
        xg_val_text = """
                            <b style="color:orange">XGBoost Validation: </b>
                            <p> Threshold: {:,.3f}  -  Precision: {:,.3f} -  Recall: {:,.3f}  -  F1 Score: {:,.3f}  -  Total Samples: {:,.0f}</p>
                            """.format(self.xg_threshold, self.xg_val_precision, self.xg_val_recall, self.xg_val_f1, len(self.xg_val_true))
        return rf_text, xg_text, rf_val_text, xg_val_text


def get_model_data(file_name):
    """If the file path exists load the pickle object else run
    the main file to generate the pickle object"""
    if os.path.exists(file_name):
        model_params = pickle.load(open(file_name, 'rb'))
        return model_params
    else:
        print("File does not exist")
        return


def main():
    """Get the data for the dashboard, if pickle files don't
    exist will generate them"""
    file_directory = os.path.dirname(os.path.abspath(__file__))
    model_file = os.path.join(file_directory, 'rf_results_dict.pkl')
    model1_params = get_model_data(model_file)
    model_file = os.path.join(file_directory, 'xgb_results_dict.pkl')
    model2_params = get_model_data(model_file)
    # print(model2_params.keys())
    dashboard = Dashboard(model1_params, model2_params)
    dashboard.run()


# You must directly call the function! no if...
main()
# test()
